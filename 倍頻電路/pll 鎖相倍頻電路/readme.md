## PLL 鎖相倍頻電路基礎
- 使用場景: 
  - MCU/CUP 的倍頻輸出
    ```
    將 n 倍的輸入頻率提升為數十倍頻輸出，
    常用於晶片內部將輸入的晶振頻率提升到指定的倍率
    ```
  - FM 訊號的解調變
  - 小信號的恢復（用鎖相放大器來追蹤參考頻率）
  - 直流電動機的電機控制器

- 需求
  
  透過`壓控震盪器(Voltage-Control-Oscillator, VCO)`，可以利用電壓輸出不同頻率的波形，
  但 VCO 無法輸出穩定的頻率的波形，只要電壓稍有浮動，就會造成頻率的變化，因此 VCO 的前一級需要輸出穩定電壓的輸出

  要使 VCO 輸出的頻率穩定，需要`檢測`輸出的頻率，並能夠適時的`調整`的機制，
  透過鑑相器，將`輸出的頻率(f2)`與`輸入的頻率(f1)`進行相位比較，
  - 若 f2 與 f1 有差異，鑑相器輸出為 HIGH
  - 若 f2 與 f1 無差異，鑑相器輸出為 LOW
  透過鑑相器的輸出可以得到一個`脈波的輸出`，脈波在高電位處代表頻率有差異

  要將脈波訊號輸出為一個穩定的直流，可以透過低通濾波器，
  低通濾波器實際上是一個簡易的`RC 電路`，會在鑑相器輸出為 HIGH 時對電容器充電，
  使得低通濾波器產生`變化率很小的電壓輸出`，此電壓在輸入給 VCO ，就可以得到穩定且預期高倍頻率的波形輸出

  - `元件1`，壓控震盪器(VCO)
    
    <img src="vco.png" width=80% height=auto>

  - `元件2`，鑑相器
    
    當 f1 和 f2 有相位差的地方，輸出為 HIGH

    <img src="phase-detector-output.png" width=80% height=auto>

    [鑑相器電路範例](https://www.analog.com/cn/design-center/landing-pages/002/tech-articles-taiwan/phase-locked-loop-pll-fundamentals.html)

    <img src="phase-detector.png" width=80% height=auto>

  - `元件3`，低通濾波器

    <img src="low-pass-filter.png" width=40% height=auto>

  - 完整電路，
    
    使輸出頻率穩定的電路(並未倍頻)
    
    <img src="stable-freq-output.png" width=80% height=auto>

    要實現倍頻，需要加上分頻器

    <img src="pll-circuit.png" width=80% height=auto>

    CD4046B 晶片為例

    <img src="CD4046B-block-diagram.png" width=80% height=auto>


## Ref
- [CD4046B datasheet](https://www.ti.com/lit/an/scha002a/scha002a.pdf)

- [鎖相迴路](https://zh.wikipedia.org/wiki/%E9%94%81%E7%9B%B8%E7%8E%AF)

- [鎖相迴路(PLL)基本原理](https://www.analog.com/cn/design-center/landing-pages/002/tech-articles-taiwan/phase-locked-loop-pll-fundamentals.html)

- [Lec28 訊號與系統 單元二十二 Phase-Locked Loop @ y2b](https://www.youtube.com/watch?v=7tZwpexlvEs)

- [06-02 PLL鎖相迴路介紹 @ y2b](https://www.youtube.com/watch?v=33brFaGVh2E)

- [让频率提升几十倍的电路 @ y2b](https://www.youtube.com/watch?v=YtlwIFsiWtE&list=PLHpxGJbAw8Iw0DJ890N-Cb-5souIPetpE&index=4)

- [鎖相迴路原理](https://www.ctimes.com.tw/DispArt/tw/%E9%8E%96%E7%9B%B8%E8%BF%B4%E8%B7%AF/%E4%B8%80%E8%88%AC%E9%82%8F%E8%BC%AF%E5%85%83%E4%BB%B6/031205175414.shtml)

