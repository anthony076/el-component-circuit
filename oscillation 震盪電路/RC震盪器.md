## 為什麼需要 RC 振盪器
- 優點
  - 價格便宜
  - 可以內嵌於晶片中

- 缺點
  - 起振到波形穩定需要一段時間才會穩定

## RC 振盪器基礎
- 需求
  - 需要回授的機制，可以判斷停止的時機
  - 需要放大的機制，避免小訊號的損耗
  - 需要頻率的過濾器，可以避免雜訊，並且只讓特定頻率通過

- 元件1，RC 振盪器的實現: OPA + 維恩電橋
  - 利用OPA的負回授，實現簡易的振盪器
    
    <img src="oscillator-by-opa.png" width=80% height=auto>
  

- 元件2，維恩電橋，允許特定頻率通過，
  
  維恩電橋由一組高通濾波器+低通濾波器組成的帶通濾波器，
  透過帶通濾波器，可以 OPA 輸出固定的頻率

  <img src="wien-bridge.png" width=100% height=auto>

  維恩電橋的輸出會衰減

  <img src="decade-of-wien-bridge.png" width=80% height=auto>

- RC 振盪器 = OPA + 維恩電橋

  由於維恩電橋會衰減訊號，因此 OPA的放大倍率 > 維恩電橋的衰減倍率，
  例如，維恩電橋衰減3倍，則 OPA 至少要大於 3 倍，
  才不至於使輸出信號越來越小，最後衰減至0

  <img src="wien-and-opa.png" width=80% height=auto>


## Ref
- [RC振盪器的工作原理，維恩電橋振盪器](https://www.youtube.com/watch?v=Qqk0FWMsUeU)
